const express = require("express");
const app = express();
const port = 2312;
const bodyParser = require("body-parser");

app.use(
  bodyParser.urlencoded({
    limit: "100mb",
    extended: true,
  })
);

app.use(bodyParser.json());

var OnOrganization = require("./handler_adam/OnOrganization");
var OnLocation = require("./handler_adam/OnLocation");
var onGenerateToken = require("./handler_adam/GenerateToken");
var OnPractioner = require("./handler_adam/OnPractioner");
var OnPatient = require("./handler_adam/OnPatient");
var OnEncounters = require("./handler_adam/Encounter");
var OnCondition = require("./handler_adam/Condition");
var Allergy = require("./handler_adam/allergy_intolenrance/Allergy");
var ClinicalImpression = require("./handler_adam/allergy_intolenrance/ClinicalImpression");
var Condition = require("./handler_adam/patient_condition/Condition");
var EncounterDischarge = require("./handler_adam/patient_condition/EncounterDischarge");
var Observation = require("./handler_adam/patient_condition/Observation");
var ProcedureEducation = require("./handler_adam/patient_condition/ProcedureEducation");
var ServiceRequestDischarge = require("./handler_adam/patient_condition/ServiceRequest");
var Radiology = require("./handler_adam/Radiology");
var Laboratory = require("./handler_adam/Laboratory");
var Observations = require("./handler_adam/Observation");
var Procedures = require("./handler_adam/Procedure");
var Compositions = require("./handler_adam/Composition");
var Medication = require("./handler_adam/Medication")
//organization
app.post("/internal/organization", (req, res) => {
  OnOrganization.OnOrganization(req, req.body, (respond) => {
    res.json(respond);
  });
});
app.put("/internal/organization/update/:id", (req, res) => {
  OnOrganization.OnUpdateOrganization(req, req.body, (respond) => {
    res.json(respond);
  });
});
app.get("/internal/organization/:id", (req, res) => {
  OnOrganization.OnGetByID(req, req.body, (respond) => {
    res.json(respond);
  });
});

//location
app.post("/internal/location", (req, res) => {
  console.log(req.body);
  OnLocation.OnLocation(req, req.body, (respond) => {
    console.log("reqbody", req.body);
    console.log("respon", respond);
    res.json(respond);
  });
});

app.put("/internal/location/update/:id", (req, res) => {
  OnLocation.OnUpdateLocation(req, req.body, (respond) => {
    res.json(respond);
  });
});
app.get("/internal/location/read/:id", (req, res) => {
  console.log("sini");
  OnLocation.OnGetByID(req, req.body, (respond) => {
    res.json(respond);
  });
});

//auth
app.post("/internal/auth", (req, res) => {
  onGenerateToken.onGenerateToken(req.body, (respond) => {
    res.json(respond);
  });
});

//practioner
app.get("/internal/Practioner/:id", (req, res) => {
  OnPractioner.OnGetById(req, req.body, (respond) => {
    res.json(respond);
  });
});
app.get("/internal/Practioner/", (req, res) => {
  OnPractioner.OnGetByNIK(req, req.body, (respond) => {
    res.json(respond);
  });
});

//patient
app.get("/internal/Patient/", (req, res) => {
  // console.log("Req query", req.query);
  OnPatient.OnGetByNIK(req, req.body, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/encounter", (req, res) => {
  OnEncounters.OnEncounter(req, req.body, (respond) => {
    res.json(respond);
  });
});

// alergi
app.post("/internal/allergy", (req, res) => {
  Allergy.OnCreateAllergy(req, (respond) => {
    res.json(respond);
  });
});

// clinical impression (kesan)
app.post("/internal/clinicalImpression", (req, res) => {
  ClinicalImpression.OnCreateClinicalImpression(req, (respond) => {
    res.json(respond);
  });
});

// condition
app.post("/internal/Condition", (req, res) => {
  Condition.OnCreateCondition(req, (respond) => {
    res.json(respond);
  });
});

// pulang
app.post("/internal/EncounterDischarge", (req, res) => {
  EncounterDischarge.OnUpdateEncounterDischarge(req, (respond) => {
    res.json(respond);
  });
});

// kesadaran
app.post("/internal/Observation", (req, res) => {
  Observation.OnCreateObservation(req, (respond) => {
    res.json(respond);
  });
});

// edukasi
app.post("/internal/ProcedureEducation", (req, res) => {
  ProcedureEducation.OnCreateProcedureEducation(req, (respond) => {
    res.json(respond);
  });
});

// discharge rujukan/kontrol
app.post("/internal/ServiceRequestDischarge", (req, res) => {
  ServiceRequestDischarge.OnCreateServiceRequest(req, (respond) => {
    res.json(respond);
  });
});

// radiologi
app.post("/internal/radiology/DiagnosticReport", (req, res) => {
  Radiology.OnCreateDiagnosticReport(req, (respond) => {
    res.json(respond);
  });
});

app.get("/internal/radiology/ImagingStudy", (req, res) => {
  Radiology.OnGetImagingStudy(req, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/radiology/Observation", (req, res) => {
  Radiology.OnCreateObservation(req, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/radiology/ServiceRequest", (req, res) => {
  Radiology.OnCreateServiceRequest(req, (respond) => {
    res.json(respond);
  });
});

//update encounter progress
app.put("/internal/encounter/update_progress/:id", (req, res) => {
  OnEncounters.OnUpdateEcounterInProgress(req, req.body, (respond) => {
    res.json(respond);
  });
});

//update encounter discharge
app.put("/internal/encounter/update_discharger/:id", (req, res) => {
  OnEncounters.OnUpdateEcounterDischargeDisposition(
    req,
    req.body,
    (respond) => {
      res.json(respond);
    }
  );
});

//update encounter finished
app.put("/internal/encounter/update_finished/:id", (req, res) => {
  OnEncounters.OnUpdateEcounterFinished(req, req.body, (respond) => {
    res.json(respond);
  });
});

//get ecounter id
app.get("/internal/encounter/read/:id", (req, res) => {
  OnEncounters.OnGetByID(req, req.body, (respond) => {
    res.json(respond);
  });
});

//get ecounter subject
app.get("/internal/encounter/read/", (req, res) => {
  OnEncounters.OnGetBySubject(req, req.body, (respond) => {
    res.json(respond);
  });
});

//condition
app.post("/internal/condition_primary", (req, res) => {
  OnCondition.OnCondition_Primary(req, req.body, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/condition_secondary", (req, res) => {
  OnCondition.OnCondition_Secondary(req, req.body, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/Observations", (req, res) => {
  Observations.OnObservation(req, req.body,(respond) => {
    res.json(respond);
  });
});

app.post("/internal/procedures", (req, res) => {
  Procedures.OnProcedure(req,req.body, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/compositions", (req, res) => {
  Compositions.OnComposition(req,req.body, (respond) => {
    res.json(respond);
  });
});









// laboratory
app.post("/internal/laboratory/service_request", (req, res) => {
  Laboratory.OnCreateServiceRequest(req, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/laboratory/specimen", (req, res) => {
  Laboratory.OnCreateSpecimen(req, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/laboratory/observation", (req, res) => {
  Laboratory.OnCreateObservation(req, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/laboratory/diagnostic_report", (req, res) => {
  Laboratory.OnCreateDiagnosticReport(req, (respond) => {
    res.json(respond);
  });
});

app.post("/internal/medication", (req, res) => {
  Medication.OnMedication(req,req.body,(respond) => {
    res.json(respond);
  });
});

app.listen(port, () => console.log(`bridging APP listen in ${port}!`));

function errorRespond(message) {
  return {
    success: false,
    message: message,
  };
}
