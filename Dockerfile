FROM node:16

WORKDIR /usr/src/app
COPY . .
COPY package*.json ./
RUN npm install
EXPOSE 2310

CMD [ "node", "index.js" ]
