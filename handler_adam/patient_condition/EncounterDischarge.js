var axios = require("axios");
const e = require("express");
var request = require("request");
require("dotenv").config();
module.exports = {
  // fase 5b
  OnUpdateEncounterDischarge: async function (req, callback) {
    var id = data.id;
    var authToken = req.headers.authorization;

    if (authToken !== undefined) {
      var data = {
        resourceType: "Encounter",
        id: req.body.id,
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/encounter/" +
              req.body.identifier_encounter_discharge,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        class: {
          system: "http://terminology.hl7.org/CodeSystem/v3-ActCode",
          code: req.body.class_code,
          display: req.body.class_display,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
          display: req.body.subject_display,
        },
        participant: req.body.participant,
        period: {
          start: req.body.period_start,
          end: req.body.period_end,
        },
        location: req.body.location,
        diagnosis: req.body.diagnosis,
        statusHistory: req.body.statusHistory,
        hospitalization: {
          dischargeDisposition: {
            coding: req.body.dischargeDisposition_coding,
            text: req.body.dischargeDisposition_text,
          },
        },
        serviceProvider: {
          reference: "Organization/" + req.body.serviceProvider_reference,
        },
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "PUT",
        url:
          "https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1/Encounter/" +
          id,
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },
};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
