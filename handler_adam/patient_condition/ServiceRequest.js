var axios = require("axios");
const e = require("express");
var request = require("request");
require("dotenv").config();
module.exports = {
  // fase 5b
  OnCreateServiceRequest: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken !== undefined) {
      var data = {
        resourceType: "ServiceRequest",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/servicerequest/" +
              req.body.identifier_servicerequest,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        intent: req.body.intent,
        priority: req.body.priority,
        category: req.body.category,
        code: {
          coding: req.body.code_coding,
          text: req.body.code_text,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
          display: req.body.encounter_display,
        },
        occurrenceDateTime: req.body.occurrenceDateTime,
        authoredOn: req.body.authoredOn,
        requester: {
          reference: "Practitioner/" + req.body.requester_reference_number,
          display: req.body.requester_display,
        },
        performer: [
          {
            reference: "Practitioner/" + req.body.performer_reference_number,
            display: req.body.performer_display,
          },
        ],
        reasonCode: [
          {
            coding: req.body.reasonCode_coding,
            text: req.body.reasonCode_text,
          },
        ],
        locationCode: [
          {
            coding: req.body.locationCode_coding,
          },
        ],
        locationReference: [
          {
            reference:
              "Location/" + req.body.locationReference_reference_number,
            display: req.body.locationReference_display,
          },
        ],
        patientInstruction: req.body.patientInstruction,
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1/ServiceRequest",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken, 
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },
};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
