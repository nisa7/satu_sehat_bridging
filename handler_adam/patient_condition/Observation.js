var axios = require("axios");
const e = require("express");
var request = require("request");
require("dotenv").config();
module.exports = {
  // fase 5b
  OnCreateObservation: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken !== undefined) {
      var data = {
        resourceType: "Observation",
        status: req.body.status,
        category: [
          {
            coding: req.body.category_coding,
          },
        ],
        code: {
          coding: req.body.code_coding,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        performer: [
          {
            reference: "Practitioner/" + req.body.performer_reference_number,
          },
        ],
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
          display: req.body.encounter_display,
        },
        effectiveDateTime: req.body.effectiveDateTime,
        issued: req.body.issuedTime,
        valueCodeableConcept: {
          coding: req.body.valueCodeableConcept_coding,
        },
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1/Observation",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },
};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
