var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');

require('dotenv').config();
module.exports = {
  OnObservation : async function(req,data,callback) {
    console.log("data");
        if(req.headers.authorization !== undefined){
          console.log("req.headers.authorization",req.headers.authorization);
          // console.log("datas",req);
          const authToken = req.headers.authorization;

          var category = [
            {
            coding : [
                {
                   system : "http://terminology.hl7.org/CodeSystem/observation-category",
                   code : data.category_code,
                   display : data.category_display
                }
             ]
            }
          ]

          var codes = {
            coding : [
               {
                  system: "http://loinc.org",
                  code: data.code_coding,
                  display: data.code_display
               }
            ]
         };

         var subject = {
            reference: data.subject_reference_number
          
         }
       
         var performer = [
            {
            reference : data.performer_reference
            }
        ]
         encounter = {
            reference: data.encounter_reference_number,
            display : data.encounter_display

         };
         console.log("valueQuantity.value");
         var valueQuantity = {
            value: data.valueQuantity.value,
            unit : data.valueQuantity.unit,
            system:  "http://unitsofmeasure.org",
            code : data.valueQuantity.code
        }

          var dataTemplete = {};
            dataTemplete = { 
              resourceType: data.resourceType,
              status : data.status,
              category :category,
              code : codes,
              subject : subject,
              performer : performer,
              encounter : encounter,
              effectiveDateTime : data.effectiveDateTime,
              issued : data.issued,
              valueQuantity: valueQuantity
            };

          console.log('\r\nSend body tempelte ')
          console.log(JSON.stringify(dataTemplete))
          var options = {
            method: "POST",
            url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Observation",
            headers: {
              "Content-Type": "application/json",
              "Authorization" : authToken
            },
            body: dataTemplete,
            json: true
          };

          request(options, async function(error, response, rows) {
            console.log('eror observation ' + error);
            console.log('eror observation ' ,response);
            console.log('eror observation ' , rows);
          //   try{
              if (error) {
                console.log("Error request")
                return callback({
                  success: false,
                  message: error,
                  payload: error
                });
              } else {
                console.log("sini");
                return callback({
                      success: true,
                      message: "berhasil",
                      payload: rows
                });
              }
            
          });
        }
        else{
          return callback({
            success: false,
            message: "token tidak ditemukan"
          });
        }

  }



};
// })

