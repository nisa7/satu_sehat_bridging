var axios = require("axios");
const e = require("express");
var request = require("request");
require("dotenv").config();
module.exports = {
  // fase 4
  OnCreateServiceRequest: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken) {
      var data = {
        resourceType: "ServiceRequest",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/servicerequest/" +
              req.body.identifier_serviceRequest,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        intent: req.body.intent,
        priority: req.body.priority,
        category: req.body.category,
        code: {
          coding: req.body.code_coding,
          text: req.body.code_text,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
          display: req.body.encounter_display,
        },
        occurrenceDateTime: req.body.occurrenceDateTime,
        authoredOn: req.body.authoredOn,
        requester: {
          reference: "Practitioner/" + req.body.requester_reference_number,
          display: req.body.requester_display,
        },
        performer: req.body.performer,
        reasonCode: req.body.reasonCode,
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/ServiceRequest",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },

  OnCreateSpecimen: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken) {
      var data = {
        resourceType: "Specimen",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/specimen/" +
              req.body.identifier_specimen,
            value: req.body.identifier_value,
            assigner: {
              reference: "Organization/" + req.body.identifier_assigner,
            },
          },
        ],
        status: req.body.status,
        type: {
          coding: req.body.type_coding,
        },
        collection: {
          method: {
            coding: req.body.method_coding,
          },
          collectedDateTime: req.body.collectedDateTime,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
          display: req.body.subject_display,
        },
        request: req.body.request,
        receivedTime: req.body.receivedTime,
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Specimen",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },

  OnCreateObservation: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken) {
      var data = {
        resourceType: "Observation",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/observation/" +
              req.body.identifier_observation,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        category: req.body.category,
        code: {
          coding: req.body.code_coding,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
        },
        effectiveDateTime: req.body.effectiveDateTime,
        issued: req.body.issuedTime,
        performer: req.body.performer,
        specimen: {
          reference: "Specimen/" + req.body.specimen_reference_number,
        },
        basedOn: req.body.basedOn,
      };

      // variant 1 = nominal, 2 = ordinal, 3 = kuantitatif, 4 = naratif
      switch (req.body.variant) {
        case 1:
          data.valueCodeableConcept = {
            coding: req.body.valueCodeableConcept_coding,
          };
          break;

        case 2:
          data.valueCodeableConcept = {
            coding: req.body.valueCodeableConcept_coding,
          };
          data.referenceRange = req.body.referenceRange;
          break;

        case 3:
          data.valueQuantity = {
            value: req.body.valueQuantity_value,
            unit: req.body.valueQuantity_unit,
            system: req.body.valueQuantity_system,
            code: req.body.valueQuantity_code,
          };
          data.interpretation = req.body.interpretation;
          data.referenceRange = req.body.referenceRange;
          break;

        case 4:
          data.valueString = req.body.valueString;
      }

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Observation",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },

  OnCreateDiagnosticReport: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken) {
      var data = {
        resourceType: "DiagnosticReport",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/diagnostic/" +
              req.body.identifier_diagnosticReport +
              "/lab",
            use: req.body.identifier_use,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        category: req.body.category,
        code: {
          coding: req.body.code_coding,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
        },
        effectiveDateTime: req.body.effectiveDateTime,
        issued: req.body.issuedTime,
        performer: req.body.performer,
        result: req.body.result,
        specimen: req.body.specimen,
        basedOn: req.body.basedOn,
      };

      if (req.body.variant == 4) {
        data.conclusion = req.body.conclusion;
      } else {
        data.conclusionCode = req.body.conclusionCode;
      }

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/DiagnosticReport",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },
};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
