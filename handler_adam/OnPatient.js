var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');
require('dotenv').config();
module.exports = {
  OnGetByNIK: async function(req,data,callback) {
    // var url = "https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1/Patient"
    var url = "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Patient"
    const authToken = req.headers.authorization;

    var respondData = await axios.get(
      url +"?identifier=" + req.query.identifier,
      { 
        headers: {
          "Content-Type": "application/json",
          Authorization : authToken
        }
      }
    );
    console.log("respondData",respondData.data);

    if(respondData.data === undefined ) {
      callback({
        success: false,
        message: "data tidak ditemukan",
        payload: respondData
      });
    }
    else{
    
      
          callback({
            success: true,
            message: "data berhasil di dapat",
            payload: respondData.data
          });
     
    } 
    
  }
};

