var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');
require('dotenv').config();
module.exports = {
  OnLocation: async function(req,data,callback) {
        var identifier = [];
        var telecom = [];
        var address = [];
        var extensions= [];
        var extension_detail = [];
        var coding = [];
        if(req.headers.authorization !== undefined){
          console.log("req.headers.authorization",req.headers.authorization);
          // console.log("datas",req);
          const authToken = req.headers.authorization;
          await loop(data.identifier,async identifiers =>{
            identifier.push({
              system : identifiers.system,
              value : identifiers.value
            });
          });
          await loop(data.telecom,async telecoms =>{
            telecom.push({
              system : telecoms.system,
              value : telecoms.value,
              use : telecoms.use
            });
          });
          await loop(data.address.extension[0].extension,async extension_data =>{
            console.log("extension_data",extension_data);
            extension_detail.push({
              url : extension_data.url,
              valueCode : extension_data.valueCode
            })
          });

          extensions.push({
            url : data.address.extension[0].url,
            extension : extension_detail
          })

          address = {
            use : data.address.user,
            line : [
                data.address.line[0]
            ],
            city : data.address.city,
            postalCode : data.address.postalCode,
            country : data.address.country,
            extension : extensions
          };

          await loop(data.physicalType.coding,async codings =>{
            coding.push({
              system : codings.system,
              code : codings.code,
              display : codings.code
             })
          });
          console.log("telecom",telecom);

          var dataTemplete = {};
            dataTemplete = { 
              resourceType: data.resourceType,
              identifier : identifier,
              status: data.status,
              name : data.name,
              description : data.description,
              mode : data.mode,
              telecom : telecom,
              address : address,
              physicalType :  {
                coding : coding
              },
              position: {
                  longitude: data.position.longitude,
                  latitude: data.position.latitude,
                  altitude: data.position.altitude
              },
              managingOrganization: {
                  reference : data.managingOrganization.reference
              }
            };

          console.log('\r\nSend body tempelte ')
          console.log(JSON.stringify(dataTemplete))
          var options = {
            method: "POST",
            url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Location",
            headers: {
              "Content-Type": "application/json",
              "Authorization" : authToken
            },
            body: dataTemplete,
            json: true
          };

          request(options, async function(error, response, rows) {
            console.log('eror' + error);
          //   try{
              if (error) {
                console.log("Error request")
                return callback({
                  success: false,
                  message: error,
                  payload: error
                });
              } else {
                return callback({
                      success: true,
                      message: "berhasil",
                      payload: rows
                });
              }
            
          });
        }
        else{
          return callback({
            success: false,
            message: "token tidak ditemukan"
          });
        }

  },
  OnUpdateLocation: async function(req,data,callback) {
    var identifier = [];
    var telecom = [];
    var address = [];
    var extensions= [];
    var extension_detail = [];
    var coding = [];
    if(req.headers.authorization !== undefined){
        console.log("req.headers.authorization",req.headers.authorization);
        // console.log("datas",req);
        const authToken = req.headers.authorization;
        await loop(data.identifier,async identifiers =>{
          identifier.push({
            system : identifiers.system,
            value : identifiers.value
          });
        });
        await loop(data.telecom,async telecoms =>{
          telecom.push({
            system : telecoms.system,
            value : telecoms.value,
            use : telecoms.use
          });
        });
        await loop(data.address.extension[0].extension,async extension_data =>{
          console.log("extension_data",extension_data);
          extension_detail.push({
            url : extension_data.url,
            valueCode : extension_data.valueCode
          })
        });

        extensions.push({
          url : data.address.extension[0].url,
          extension : extension_detail
        })

        address = {
          use : data.address.address,
          line : [
              data.address.line[0]
          ],
          city : data.address.city,
          postalCode : data.address.postalCode,
          country : data.address.country,
          extension : extensions
        };

        await loop(data.physicalType.coding,async codings =>{
          coding.push({
            system : codings.system,
            code : codings.code,
            display : codings.code
          })
        });
        console.log("telecom",telecom);


        var dataTemplete = {};
        dataTemplete = { 
          id : req.params.id,
          resourceType: data.resourceType,
          identifier : identifier,
          status: data.status,
          name : data.name,
          description : data.description,
          mode : data.mode,
          telecom : telecom,
          address : address,
          physicalType :  {
            coding : coding
          },
          position: {
              longitude: data.position.longitude,
              latitude: data.position.latitude,
              altitude: data.position.altitude
          },
          managingOrganization: {
              reference : data.managingOrganization.reference
          }
        };

      console.log('\r\nSend body tempelte ')
      console.log(JSON.stringify(dataTemplete))
      var options = {
        method: "PUT",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Location/"+ req.params.id,
        headers: {
          "Content-Type": "application/json",
          "Authorization" : authToken
        },
        body: dataTemplete,
        json: true
      };

      request(options, async function(error, response, rows) {
        console.log('eror' + error);
      //   try{
          if (error) {
            console.log("Error request")
            return callback({
              success: false,
              message: error,
              payload: error
            });
          } else {
            return callback({
                  success: true,
                  message: "berhasil",
                  payload: rows
            });
          }
        
      });

    }
    else{
      return callback({
        success: false,
        message: "token tidak ditemukan"
      });
    }


  // }
  // else{
  //   callback({
  //     success: false,
  //     message: "url bridging registrasi kosong",
  //     payload: null
  //   });
  // }
// }
// else{
//   callback({
//     success: false,
//     message: "status oem non aktif",
//     payload: "status oem non aktif"
//   });  
// }
  },
  OnGetByID: async function(req,data,callback) {
    var url = "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Location/"
    console.log("url",url);
    const authToken = req.headers.authorization;
    var respondData = await axios.get(
      url +req.params.id ,
      { 
        headers: {
          "Content-Type": "application/json",
          "Authorization" : authToken
        }
      }
    );
    // console.log("respondData",respondData);

    if(respondData.data.id === undefined ) {
      callback({
        success: false,
        message: "data tidak ditemukan",
        payload: respondData
      });
    }
    else{
    
      
          callback({
            success: true,
            message: "data berhasil di dapat",
            payload: respondData.data
          });
     
    } 
     

    
  }
};
// })
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
