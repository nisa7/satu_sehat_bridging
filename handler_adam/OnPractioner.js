var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require("mysql");
require("dotenv").config();
module.exports = {
  OnGetByNIK: async function (req, data, callback) {
    var url =
      "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Practitioner";
    const authToken = req.headers.authorization;
    var respondData = await axios.get(
      url + "?identifier=" + req.query.identifier,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
      }
    );
    console.log("respondData", respondData.data);

    if (respondData.data === undefined) {
      callback({
        success: false,
        message: "data tidak ditemukan",
        payload: respondData,
      });
    } else {
      callback({
        success: true,
        message: "data berhasil di dapat",
        payload: respondData.data,
      });
    }
  },
  OnGetById: async function (req, data, callback) {
    var url =
      "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Practitioner/";
    const authToken = req.headers.authorization;
    var respondData = await axios.get(url + req.params.id, {
      headers: {
        "Content-Type": "application/json",
        Authorization: authToken,
      },
    });
    console.log("respondData", respondData.data);

    if (respondData.data.id === undefined) {
      callback({
        success: false,
        message: "data tidak ditemukan",
        payload: respondData,
      });
    } else {
      callback({
        success: true,
        message: "data berhasil di dapat",
        payload: respondData.data,
      });
    }
  },
};
// })
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
