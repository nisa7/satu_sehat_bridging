var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');

require('dotenv').config();
module.exports = {
  OnComposition : async function(req,data,callback) {
   
        var coding_type = [];
        var author = [];
        var sections_array = [];
        var coding_section = [];
      if(req.headers.authorization !== undefined){
          console.log("req.headers.authorization",req.headers.authorization);
          // console.log("datas",req);
          const authToken = req.headers.authorization;

          var identifier = {
            system : data.identifier_system,
            value : data.identifier_value
          }

          coding_type.push({
            system : "http://loinc.org",
            code : data.type_code,
            display : data.type_display
          })


          var type ={
            coding : coding_type
          }


          var category = [
            {
            coding : [
                {
                   system : "http://loinc.org",
                   code : data.category_code,
                   display : data.category_display
                }
             ]
            }
          ]

         var subject = {
            reference: data.subject_reference_number,
            display : data.subject_display
         }
         encounter = {
            reference: data.encounter_reference_number,
            display : data.encounter_display

         };
         author.push({
            reference : data.author_reference_number,
            display :  data.author_display
         });
         var custodian = {
            reference : data.custodian_reference_number
         };

        await loop(data.section,async sections_object =>{
          await loop(sections_object.code.coding,async codings_object =>{
            coding_section.push({
              system: "http://loinc.org",
              code :codings_object.code,
              display : codings_object.display
            })
          })
  
        });
        await loop(data.section,async sections_object2 =>{
          sections_array.push({
            code : {
              coding :coding_section
            },
            text : {
              status : sections_object2.text.status,
              div : sections_object2.text.div
            }            
          })
        });
    
          var dataTemplete = {};
            dataTemplete = { 
              resourceType: data.resourceType,
              identifier : identifier,
              status : data.status,
              type : type,
              category :category,
              subject : subject,
              encounter : encounter,
              date : data.date,
              author : author,
              title : data.title,
              custodian :custodian,
              section : sections_array
            };

          console.log('\r\nSend body tempelte ')
          console.log(JSON.stringify(dataTemplete))
          var options = {
            method: "POST",
            url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Composition",
            headers: {
              "Content-Type": "application/json",
              "Authorization" : authToken
            },
            body: dataTemplete,
            json: true
          };

          request(options, async function(error, response, rows) {
            console.log('eror compotion ' + error);
          //   try{
              if (error) {
                console.log("Error request")
                return callback({
                  success: false,
                  message: error,
                  payload: error
                });
              } else {
                return callback({
                      success: true,
                      message: "berhasil",
                      payload: rows
                });
              }
          });
        }
        else{
          return callback({
            success: false,
            message: "token tidak ditemukan"
          });
        }

      }
    };

// })


async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}

