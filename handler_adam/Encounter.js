var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require("mysql");
const { LATIN7_ESTONIAN_CS } = require("mysql2/lib/constants/charsets");
require("dotenv").config();
module.exports = {
  OnEncounter: async function (req, data, callback) {
    var identifierss = [];
    var telecom = [];
    var address = [];
    var extensions = [];
    var participants = [];
    var codings = [];
    var type = [];
    var locationss = [];
    var statusHistory = [];
    if (req.headers.authorization !== undefined) {
      console.log("req.headers.authorization", req.headers.authorization);
      // console.log("datas",req);
      const authToken = req.headers.authorization;
        identifierss.push({
          system: data.identifier_system,
          value: data.identifier_value,
        });
      // });
        var kelas = {
          system: "http://terminology.hl7.org/CodeSystem/v3-ActCode",
          code: data.class_code,
          display: data.class_display,
        };

      var subject = {
        reference: data.subject_reference,
        display: data.subject_display,
      };

      var periode = {
        start: data.period_start,
      };
      await loop(data.participant, async (participant) => {
        await loop(participant.type, async (type) => {
          // await loop(type.coding, async (coding) => {
            codings.push({
              system: "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
              code: type.coding_code,
              display: type.coding_display,
            });
          // });
        });
      });

      await loop(data.participant, async (participant) => {
        participants.push({
          type: [
            {
              coding: codings,
            },
          ],
          individual: {
            reference: participant.individual.reference,
            display: participant.individual.display,
          },
        });
      });

     
        locationss.push({
          location: {
            reference: data.location_reference,
            display: data.location_display,
          },
    
      });


      await loop(data.statusHistory, async (statusHistorys) => {
        statusHistory.push({
          status: statusHistorys.status,
          period: {
            start: statusHistorys.period.start,
            end: statusHistorys.period.end,
          },
        });
      });

      var serviceProvider = {
        reference: data.serviceProvider_reference,
      };
      var dataTemplete = {};
      dataTemplete = {
        resourceType: data.resourceType,
        status: data.status,
        class: kelas,
        subject: subject,
        participant: participants,
        period: periode,
        location: locationss,
        statusHistory: statusHistory,
        serviceProvider: serviceProvider,
        identifier: identifierss,
      };

      console.log("\r\nSend body tempelte ");
      console.log(JSON.stringify(dataTemplete));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Encounter",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: dataTemplete,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
            payload: error,
          });
        } else {
          return callback({
            success: true,
            message: "berhasil",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "token tidak ditemukan",
      });
    }
  },
  OnUpdateEcounterInProgress: async function (req, data, callback) {
    var identifierss = [];
    var telecom = [];
    var address = [];
    var extensions = [];
    var participants = [];
    var codings = [];
    var type = [];
    var locationss = [];
    var statusHistory = [];
    if (req.headers.authorization !== undefined) {
      console.log("req.headers.authorization", req.headers.authorization);
      // console.log("datas",req);
      const authToken = req.headers.authorization;
      // await loop(data.identifier, async (identifiers) => {
        identifierss.push({
          system: data.identifier_system,
          value: data.identifier_value,
        });
      // });
      var kelas = { 
        system: "http://terminology.hl7.org/CodeSystem/v3-ActCode",
        code: data.class_code,
        display: data.class_display,
      };

      var subject = {
        reference: data.subject_reference,
        display: data.subject_display,
      };

      var periode = {
        start: data.period_start
      };
      await loop(data.participant, async (participant) => {
        await loop(participant.type, async (type) => {
        
            codings.push({
              system: "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
              code: data.coding_code,
              display: data.coding_display,
            });
    
        });
      });

      await loop(data.participant, async (participant) => {
        participants.push({
          type: [
            {
              coding: codings,
            },
          ],
          individual: {
            reference: participant.individual.reference,
            display: participant.individual.display,
          },
        });
      });

      // console.log("data.location", data.location);
      // await loop(data.location, async (locations) => {
        locationss.push({
          location: {
            reference: data.location_reference,
            display: data.location_display,
          },
        });
      // });
      // console.log("locationss", locationss);

      await loop(data.statusHistory, async (statusHistorys) => {
        statusHistory.push(
          {
            status: statusHistorys.status,
            period: {
              start: statusHistorys.period.start,
              end: statusHistorys.period.end,
            },
          },
          {
            status: statusHistorys.status,
            period: {
              start: statusHistorys.period.start,
              end: statusHistorys.period.end,
            },
          }
        );
      });

      var serviceProvider = {
        reference: data.serviceProvider_reference,
      };
      var dataTemplete = {};
      dataTemplete = {
        resourceType: data.resourceType,
        id: data.id,
        identifier: identifierss,
        status: data.status,
        class: kelas,
        subject: subject,
        participant: participants,
        period: periode,
        location: locationss,
        statusHistory: statusHistory,
        serviceProvider: serviceProvider,
      };

      console.log("\r\nSend body tempelte ");
      console.log(JSON.stringify(dataTemplete));
      var options = {
        method: "PUT",
        url:
          "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Encounter/" +
          req.params.id,
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: dataTemplete,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
            payload: error,
          });
        } else {
          return callback({
            success: true,
            message: "berhasil",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "token tidak ditemukan",
      });
    }
  },
  OnUpdateEcounterDischargeDisposition: async function (req, data, callback) {
    var identifierss = [];
    var telecom = [];
    var address = [];
    var extensions = [];
    var participants = [];
    var codings = [];
    var type = [];
    var coding = [];
    var locationss = [];
    var statusHistory = [];
    var codings_disposition = [];
    if (req.headers.authorization !== undefined) {
      console.log("req.headers.authorization", req.headers.authorization);
      // console.log("datas",req);
      const authToken = req.headers.authorization;
      await loop(data.identifier, async (identifiers) => {
        identifierss.push({
          system: identifiers.system,
          value: identifiers.value,
        });
      });
      var kelas = {
        system: data.class.system,
        code: data.class.code,
        display: data.class.display,
      };

      var subject = {
        reference: data.subject.reference,
        display: data.subject.display,
      };

      var periode = {
        start: data.period.start,
        end: data.period.end,
      };
      await loop(data.participant, async (participant) => {
        await loop(participant.type, async (type) => {
          await loop(type.coding, async (coding) => {
            codings.push({
              system: coding.system,
              code: coding.code,
              display: coding.display,
            });
          });
        });
      });

      await loop(data.participant, async (participant) => {
        participants.push({
          type: [
            {
              coding: codings,
            },
          ],
          individual: {
            reference: participant.individual.reference,
            display: participant.individual.display,
          },
        });
      });

      console.log("data.location", data.location);
      await loop(data.location, async (locations) => {
        locationss.push({
          location: {
            reference: locations.location.reference,
            display: locations.location.display,
          },
        });
      });
      console.log("locationss", locationss);

      await loop(data.statusHistory, async (statusHistorys) => {
        statusHistory.push(
          {
            status: statusHistorys.status,
            period: {
              start: statusHistorys.period.start,
              end: statusHistorys.period.end,
            },
          },
          {
            status: statusHistorys.status,
            period: {
              start: statusHistorys.period.start,
              end: statusHistorys.period.end,
            },
          }
        );
      });
      await loop(
        data.hospitalization.dischargeDisposition.coding,
        async (codings) => {
          coding.push({
            system: codings.system,
            code: codings.code,
            display: codings.display,
          });
        }
      );

      var hospitalization = {
        dischargeDisposition: {
          coding,
          text: data.hospitalization.dischargeDisposition.text,
        },
      };

      var serviceProvider = {
        reference: data.serviceProvider.reference,
      };
      var dataTemplete = {};
      dataTemplete = {
        resourceType: data.resourceType,
        id: data.id,
        identifier: identifierss,
        status: data.status,
        class: kelas,
        subject: subject,
        participant: participants,
        period: periode,
        location: locationss,
        statusHistory: statusHistory,
        hospitalization: hospitalization,
        serviceProvider: serviceProvider,
      };

      console.log("\r\nSend body tempelte ");
      console.log(JSON.stringify(dataTemplete));
      var options = {
        method: "PUT",
        url:
          "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Encounter/" +
          req.params.id,
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: dataTemplete,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
            payload: error,
          });
        } else {
          return callback({
            success: true,
            message: "berhasil",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "token tidak ditemukan",
      });
    }
  },
  OnUpdateEcounterFinished: async function (req, data, callback) {
    var identifierss = [];
    var telecom = [];
    var address = [];
    var extensions = [];
    var participants = [];
    var codings = [];
    var coding_diagonis = [];
    var type = [];
    var locationss = [];
    var statusHistory = [];
    var codings_disposition = [];
    var diagnosiss = [];
    if (req.headers.authorization !== undefined) {
      console.log("req.headers.authorization", req.headers.authorization);
      // console.log("datas",req);
      const authToken = req.headers.authorization;
      await loop(data.identifier, async (identifiers) => {
        identifierss.push({
          system: identifiers.system,
          value: identifiers.value,
        });
      });
      var kelas = {
        system: data.class.system,
        code: data.class.code,
        display: data.class.display,
      };

      var subject = {
        reference: data.subject.reference,
        display: data.subject.display,
      };

      var periode = {
        start: data.period.start,
        end: data.period.end,
      };
      await loop(data.participant, async (participant) => {
        await loop(participant.type, async (type) => {
          await loop(type.coding, async (coding) => {
            codings.push({
              system: coding.system,
              code: coding.code,
              display: coding.display,
            });
          });
        });
      });

      await loop(data.participant, async (participant) => {
        participants.push({
          type: [
            {
              coding: codings,
            },
          ],
          individual: {
            reference: participant.individual.reference,
            display: participant.individual.display,
          },
        });
      });

      console.log("data.location", data.location);
      await loop(data.location, async (locations) => {
        locationss.push({
          location: {
            reference: locations.location.reference,
            display: locations.location.display,
          },
        });
      });
      console.log("locationss", locationss);

      await loop(data.statusHistory, async (statusHistorys) => {
        statusHistory.push(
          {
            status: statusHistorys.status,
            period: {
              start: statusHistorys.period.start,
              end: statusHistorys.period.end,
            },
          },
          {
            status: statusHistorys.status,
            period: {
              start: statusHistorys.period.start,
              end: statusHistorys.period.end,
            },
          }
        );
      });

      await loop(data.diagnosis, async (diagnosa) => {
        await loop(diagnosa.use.coding, async (data_coding) => {
          coding_diagonis.push({
            system: data_coding.system,
            code: data_coding.code,
            display: data_coding.display,
          });
        });
      });

      await loop(data.diagnosis, async (diagnosa) => {
        diagnosiss.push({
          condition: {
            reference: diagnosa.condition.reference,
            display: diagnosa.condition.display,
          },
          use: {
            coding: coding_diagonis,
          },
          rank: diagnosa.rank,
        });
      });

      // await loop(data.hospitalization.dischargeDisposition.coding , async codings => {
      //   codings_disposition.push({
      //     system : codings.system,
      //     code : codings.code,
      //     display : codings.display
      //   });
      // });

      // var hospitalization = {
      //   dischargeDisposition :{
      //     codings_disposition,
      //     text : data.hospitalization.dischargeDisposition.text
      //   }
      // }

      var serviceProvider = {
        reference: data.serviceProvider.reference,
      };
      var dataTemplete = {};
      dataTemplete = {
        resourceType: data.resourceType,
        id: data.id,
        identifier: identifierss,
        status: data.status,
        class: kelas,
        subject: subject,
        participant: participants,
        period: periode,
        location: locationss,
        diagnosis: diagnosiss,
        statusHistory: statusHistory,
        serviceProvider: serviceProvider,
      };

      console.log("\r\nSend body tempelte ");
      console.log(JSON.stringify(dataTemplete));
      var options = {
        method: "PUT",
        url:
          "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Encounter/" +
          req.params.id,
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: dataTemplete,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
            payload: error,
          });
        } else {
          return callback({
            success: true,
            message: "berhasil",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "token tidak ditemukan",
      });
    }
  },
  OnGetByID: async function (req, data, callback) {
    var url =
      "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Encounter/";
    const token = req.headers.authorization;
    console.log("req.params.id", req.params.id);
    var respondData = await axios.get(url + req.params.id, {
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    });
    console.log("respondData", respondData);

    if (respondData.id !== undefined) {
      callback({
        success: false,
        message: "ada kesalahan pada api bridigng simrs",
        payload: respondData,
      });
    } else {
      // var data = {
      //   name :"RAJAL TERPADU",
      //   id : "abddd50b-b22f-4d68-a1c3-d2c29a27698b"
      // }

      callback({
        success: true,
        message: "data berhasil di dapat",
        payload: respondData.data,
      });
    }
  },
  OnGetBySubject: async function (req, data, callback) {
    var url = "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Encounter";
    const token = req.headers.authorization;
    console.log("req.query.subject", req.query.subject);
    var respondData = await axios.get(url + "?subject=" + req.query.subject, {
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    });
    console.log("respondData", respondData);

    if (respondData.id !== undefined) {
      callback({
        success: false,
        message: "ada kesalahan pada api bridigng simrs",
        payload: respondData,
      });
    } else {
      // var data = {
      //   name :"RAJAL TERPADU",
      //   id : "abddd50b-b22f-4d68-a1c3-d2c29a27698b"
      // }

      callback({
        success: true,
        message: "data berhasil di dapat",
        payload: respondData.data,
      });
    }
  },
};
// })
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
