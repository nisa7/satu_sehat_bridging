var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');

require('dotenv').config();
module.exports = {
  OnProcedure : async function(req,data,callback) {

        var performer = [];
        var reasonCode = [];
        var bodySite = [];
        var coding_reasonCode = [];
        var coding_bodySite = [];
        var coding_categoty = [];
        var note = [];
        if(req.headers.authorization !== undefined){
          console.log("req.headers.authorization",req.headers.authorization);
          // console.log("datas",req);
          const authToken = req.headers.authorization;
          await loop (data.category.coding,async codings =>{
            coding_categoty.push({
                system : "http://snomed.info/sct",
                code :codings.code,
                display : codings.display
            })
          });
          var category = 
          {
            coding : coding_categoty,
            text : data.category.text
          }
          

          var codes = {
            coding : [
               {
                  system: "http://hl7.org/fhir/sid/icd-9-cm",
                  code: data.code_coding,
                  display: data.code_display
               }
            ]
         };

         var subject = {
            reference: data.subject_reference_number,
            display : data.subject_display
         }
         encounter = {
            reference: data.encounter_reference_number,
            display : data.encounter_display

         };
         

         var performedPeriod = {
            start : data.performedPeriod_start,
            end : data.performedPeriod_end
         }

         performer.push({
            actor :{
                reference : data.performer_actor_reference,
                display : data.performer_actor_display
            }
         });

         coding_reasonCode.push({
            system : "http://hl7.org/fhir/sid/icd-10",
            code : data.reasonCode_code,
            display : data.reasonCode_display
         })

         reasonCode.push({
            coding : coding_reasonCode
         })
         
         coding_bodySite.push({
            system: "http://snomed.info/sct",
            code: data.bodySite_code,
            display : data.bodySite_display
         })
         bodySite.push({
            coding : coding_bodySite
        }) 

        note.push({
            text : data.note_text
        });
         


          var dataTemplete = {};
            dataTemplete = { 
              resourceType: data.resourceType,
              status : data.status,
              category :category,
              code : codes,
              subject : subject,
              encounter : encounter,
              performedPeriod : performedPeriod,
              performer : performer,
              reasonCode : reasonCode,
              bodySite : bodySite,
              note : note
            };

          console.log('\r\nSend body tempelte ')
          console.log(JSON.stringify(dataTemplete))
          var options = {
            method: "POST",
            url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Procedure",
            headers: {
              "Content-Type": "application/json",
              "Authorization" : authToken
            },
            body: dataTemplete,
            json: true
          };

          request(options, async function(error, response, rows) {
            console.log('eror procedure ' + error);
          //   try{
              if (error) {
                console.log("Error request")
                return callback({
                  success: false,
                  message: error,
                  payload: error
                });
              } else {
                return callback({
                      success: true,
                      message: "berhasil",
                      payload: rows
                });
              }
            
          });
        }
        else{
          return callback({
            success: false,
            message: "token tidak ditemukan"
          });
        }

  }

};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
// })

