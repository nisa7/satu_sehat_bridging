var axios = require("axios");
const e = require("express");
var request = require("request");
require("dotenv").config();
module.exports = {
  // fase 5
  OnCreateClinicalImpression: async function (req, callback) {
    var authToken = req.headers.authorization;
    var investigation = [];
    var finding = [];

    if (authToken !== undefined) {
      await loop(req.body.investigation, async (inv) => {
        investigation.push({
          code: {
            text: inv.code_text,
          },
          item: inv.item,
        });
      });

      await loop(req.body.finding, async (find) => {
        finding.push({
          itemCodeableConcept: {
            coding: find.itemCodeableConcept_coding,
          },
          itemReference: find.itemReference,
        });
      });

      var data = {
        resourceType: "ClinicalImpression",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/clinicalimpression/" +
              req.body.identifier_clinicalimpression,
            use: req.body.identifier_use,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        description: req.body.description,
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
          display: req.body.subject_display,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
          display: req.body.encounter_display,
        },
        effectiveDateTime: req.body.effectiveDateTime,
        date: req.body.date,
        assessor: {
          reference: "Practitioner/" + req.body.assessor_reference_number,
        },
        problem: req.body.problem,
        investigation: investigation,
        summary: req.body.summary,
        finding: finding,
        prognosisCodeableConcept: req.body.prognosisCodeableConcept,
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/ClinicalImpression",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "token tidak ditemukan",
      });
    }
  },
};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
