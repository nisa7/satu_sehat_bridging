var axios = require("axios");
const e = require("express");
var request = require("request");
require("dotenv").config();
module.exports = {
  // fase 5
  OnCreateAllergy: async function (req, callback) {
    var authToken = req.headers.authorization;

    // console.log("post body = ", JSON.stringify(req.body));

    if (authToken !== undefined) {
      var data = {
        resourceType: "AllergyIntolerance",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/allergy/" +
              req.body.identifier_allergy,
            use: req.body.identifier_use,
            value: req.body.identifier_value,
          },
        ],
        clinicalStatus: {
          coding: [
            {
              system:
                "http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical",
              code: req.body.clinicalStatus_code,
              display: req.body.clinicalStatus_display,
            },
          ],
        },
        verificationStatus: {
          coding: [
            {
              system:
                "http://terminology.hl7.org/CodeSystem/allergyintolerance-verification",
              code: req.body.verificationStatus_code,
              display: req.body.verificationStatus_display,
            },
          ],
        },
        category: req.body.category,
        code: {
          coding: [
            {
              system: "http://snomed.info/sct",
              code: req.body.allergyCoding_code,
              display: req.body.allergyCoding_display,
            },
          ],
          text: req.body.allergyText,
        },
        patient: {
          reference: "Patient/" + req.body.patient_reference_number,
          display: req.body.patient_display,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
          display: req.body.encounter_display,
        },
        recordedDate: req.body.recordedDate,
        recorder: {
          reference: "Practitioner/" + req.body.recorder_reference,
        },
      };

      console.log("send body = ", JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/AllergyIntolerance",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "token tidak ditemukan",
      });
    }
  },
};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
