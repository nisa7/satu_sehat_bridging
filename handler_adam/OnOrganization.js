var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');
require('dotenv').config();
module.exports = {
  OnOrganization: async function(req,data,callback) {
        var identifier = [];
        var type = [];
        var telecom = [];
        var address = [];
        var extension= [];
        var extensions= [];
        var coding = [];
        var extension_detail = [];
        if(req.headers.authorization !== undefined){
          const authToken = req.headers.authorization;
          await loop(data.identifier,async identifiers =>{
            identifier.push({
              use: identifiers.use,
              system :identifiers.system ,
              value : identifiers.value
            });
          });
          await loop(data.telecom,async telecoms =>{
            telecom.push({
              system : telecoms.system,
              value : telecoms.value,
              use : telecoms.use
            });
          });
          await loop(data.type[0].coding,async codings=>{
            coding.push({
              system : codings.system,
              code : codings.code,
              display : coding.display
            })
          })
          type.push({
            coding : coding
          })
          extension.push({
              url : "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
              extension : [
                  {
                      url : "province",
                      valueCode : "31"
                  }
              ]

          })
          await loop(data.address[0].extension[0].extension,async extension_data =>{
            console.log("extension_data",extension_data);
            extension_detail.push({
              url : extension_data.url,
              valueCode : extension_data.valueCode
            })
          });
          extensions.push({
            url : data.address[0].extension[0].url,
            extension : extension_detail
          })
          
          address.push({
            use : data.address[0].use,
            type : data.address[0].type,
            line : [
                data.address[0].line[0]
            ],
            city : data.address[0].city,
            postalCode : data.address[0].postalCode,
            country : data.address[0].country,
            extension : extensions
          });
    
          var dataTemplete = {};
          dataTemplete = { 
            resourceType: data.resourceType,
            active: data.active,
            identifier : identifier,
            type : type,
            name: data.name,
            telecom : telecom,
            address : address,
            partOf : {
              reference : data.partOf.reference
            }
          };

        console.log('\r\nSend body tempelte ')
        console.log(JSON.stringify(dataTemplete))
        console.log("authToken",authToken);
        var options = {
          method: "POST",
          url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Organization",
          headers: {
            "Content-Type": "application/json",
            "Authorization" : authToken
          },
          body: dataTemplete,
          json: true
        };

        request(options, async function(error, response, rows) {
          console.log('eror' + error);
        //   try{
            if (error) {
              console.log("Error request")
              return callback({
                success: false,
                message: error,
                payload: error
              });
            } else {
              return callback({
                    success: true,
                    message: "berhasil",
                    payload: rows
              });
            }
      
          
        });
        }
        else{
          return callback({
            success: false,
            message: "token tidak ditemukan"
          });
        }


  },
  OnUpdateOrganization: async function(req,data,callback) {
    var identifier = [];
    var type = [];
    var telecom = [];
    var address = [];
    var extension= [];
    var extensions= [];
    var coding = [];
    var extension_detail = [];
    var  authToken = req.headers.authorization;
    await loop(data.identifier,async identifiers =>{
      identifier.push({
        use: identifiers.use,
        system :identifiers.system ,
        value : identifiers.value
      });
    });
    await loop(data.telecom,async telecoms =>{
      telecom.push({
        system : telecoms.system,
        value : telecoms.value,
        use : telecoms.use
      });
    });
    await loop(data.type[0].coding,async codings=>{
      coding.push({
        system : codings.system,
        code : codings.code,
        display : coding.display
      })
    })
    type.push({
      coding : coding
    })
    extension.push({
        url : "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
        extension : [
            {
                url : "province",
                valueCode : "31"
            }
        ]

    })
    await loop(data.address[0].extension[0].extension,async extension_data =>{
      console.log("extension_data",extension_data);
      extension_detail.push({
        url : extension_data.url,
        valueCode : extension_data.valueCode
      })
    });
    extensions.push({
      url : data.address[0].extension[0].url,
      extension : extension_detail
    })
    
    address.push({
      use : data.address[0].use,
      type : data.address[0].type,
      line : [
          data.address[0].line[0]
      ],
      city : data.address[0].city,
      postalCode : data.address[0].postalCode,
      country : data.address[0].country,
      extension : extensions
    });
    var dataTemplete = {};
      dataTemplete = { 
        resourceType: data.resourceType,
        id : data.id,
        active: data.active,
        identifier : identifier,
        type : type,
        name: data.name,
        telecom : telecom,
        address : address,
        partOf : {
          reference : data.partOf.reference
        }
      };

    console.log('\r\nSend body tempelte ')
    console.log(JSON.stringify(dataTemplete))
    const baseUrl = 'https://jsonplaceholder.typicode.com/posts';
    const id = 1;
    var options = {
      method: "PUT",
      url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Organization/"+ req.params.id,
      headers: {
        "Content-Type": "application/json",
        "Authorization" : authToken
      },
      body: dataTemplete,
      json: true
    };

    request(options, async function(error, response, rows) {
      console.log('eror' + error);
    //   try{
        if (error) {
          console.log("Error request")
          return callback({
            success: false,
            message: error,
            payload: error
          });
        } else {
          return callback({
                success: true,
                message: "berhasil",
                payload: rows
          });
        }
    //   }
    //   catch(error){
    //     return callback({
    //       success: false,
    //       message: 'sambungan api terbitkan simrs terputus, cek api simrs',
    //       payload: 'sambungan api terbitkan simrs terputus, cek api simrs' + error
    //   });
    //   }
      
    });
  // }
  // else{
  //   callback({
  //     success: false,
  //     message: "url bridging registrasi kosong",
  //     payload: null
  //   });
  // }
// }
// else{
//   callback({
//     success: false,
//     message: "status oem non aktif",
//     payload: "status oem non aktif"
//   });  
// }
  },
  OnGetByID: async function(req,data,callback) {
    var url = "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Organization/"
    const token = req.headers.authorization;
    console.log("req.params.id",req.params.id);
    var respondData = await axios.get(
      url + req.params.id,
      {
        headers: {
          "Content-Type": "application/json",
          "Authorization" : token
        }
      }
    );
    console.log("respondData",respondData);

    if(respondData.id !== undefined ) {
      callback({
        success: false,
        message: "ada kesalahan pada api bridigng simrs",
        payload: respondData
      });
    }
    else{
         
          callback({
            success: true,
            message: "data berhasil di dapat",
            payload: respondData.data
          });
     
    } 
     

    
  }

};
// })
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
