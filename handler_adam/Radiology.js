var axios = require("axios");
const e = require("express");
var request = require("request");
require("dotenv").config();
module.exports = {
  // fase 6
  OnCreateServiceRequest: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken) {
      var data = {
        resourceType: "ServiceRequest",
        identifier: req.body.identifier,
        status: req.body.status,
        intent: req.body.intent,
        priority: req.body.priority,
        category: req.body.category,
        code: {
          coding: req.body.code_coding,
          text: req.body.code_text,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
        },
        occurrenceDateTime: req.body.occurrenceDateTime,
        authoredOn: req.body.authoredOn,
        requester: {
          reference: "Practitioner/" + req.body.requester_reference_number,
          display: req.body.requester_display,
        },
        performer: req.body.performer,
        bodySite: req.body.bodySite,
        reasonCode: req.body.reasonCode,
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/ServiceRequest",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },

  OnCreateObservation: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken) {
      var data = {
        resourceType: "Observation",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/observation/" +
              req.body.identifier_observation,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        category: req.body.category,
        code: {
          coding: req.body.code_coding,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
        },
        effectiveDateTime: req.body.effectiveDateTime,
        issued: req.body.issuedTime,
        performer: req.body.performer,
        basedOn: req.body.basedOn,
        bodySite: {
          coding: req.body.bodySite_coding,
        },
        derivedFrom: req.body.derivedFrom,
        valueString: req.body.valueString,
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Observation",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },

  OnGetImagingStudy: async function (req, callback) {
    var url =
      "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/ImagingStudy";
    const authToken = req.headers.authorization;

    if (authToken) {
      var respondData = await axios.get(
        url + "?identifier=" + req.query.identifier,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: authToken,
          },
        }
      );
      console.log("respondData", respondData.data);

      if (respondData.data === undefined) {
        callback({
          success: false,
          message: "data tidak ditemukan",
          payload: respondData,
        });
      } else {
        callback({
          success: true,
          message: "data berhasil di dapat",
          payload: respondData.data,
        });
      }
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },

  OnCreateDiagnosticReport: async function (req, callback) {
    var authToken = req.headers.authorization;

    if (authToken) {
      var data = {
        resourceType: "DiagnosticReport",
        identifier: [
          {
            system:
              "http://sys-ids.kemkes.go.id/diagnostic/" +
              req.body.identifier_diagnostic +
              "/rad",
            use: req.body.identifier_use,
            value: req.body.identifier_value,
          },
        ],
        status: req.body.status,
        category: req.body.category,
        code: {
          coding: req.body.code_coding,
        },
        subject: {
          reference: "Patient/" + req.body.subject_reference_number,
        },
        encounter: {
          reference: "Encounter/" + req.body.encounter_reference_number,
        },
        effectiveDateTime: req.body.effectiveDateTime,
        issued: req.body.issuedTime,
        performer: req.body.performer,
        imagingStudy: req.body.imagingStudy,
        result: req.body.result,
        basedOn: req.body.basedOn,
        conclusion: req.body.conclusion,
      };

      // console.log(JSON.stringify(data));
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/DiagnosticReport",
        headers: {
          "Content-Type": "application/json",
          Authorization: authToken,
        },
        body: data,
        json: true,
      };

      request(options, async function (error, response, rows) {
        console.log("eror" + error);
        //   try{
        if (error) {
          console.log("Error request");
          return callback({
            success: false,
            message: error,
          });
        } else {
          return callback({
            success: true,
            message: "data berhasil ditambahkan",
            payload: rows,
          });
        }
      });
    } else {
      return callback({
        success: false,
        message: "Token tidak ditemukan",
      });
    }
  },
};
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
