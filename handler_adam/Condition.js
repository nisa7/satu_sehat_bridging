var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');

require('dotenv').config();
module.exports = {
  OnCondition_Primary: async function(req,data,callback) {
        if(req.headers.authorization !== undefined){
          console.log("req.headers.authorization",req.headers.authorization);
          // console.log("datas",req);
          const authToken = req.headers.authorization;
          var clinicalStatus = {
            coding : [
                {
                    system : "http://terminology.hl7.org/CodeSystem/condition-clinical",
                    code : data.clinicalStatus_code,
                    display : data.clinicalStatus_display
                }
            ]

          }
          var category = [
            {
            coding : [
                {
                   system : "http://terminology.hl7.org/CodeSystem/condition-category",
                   code : data.category_code,
                   display : data.category_display
                }
             ]
            }
          ]

          var code = {
            coding : [
               {
                  system: "http://hl7.org/fhir/sid/icd-10",
                  code: data.code_coding,
                  display: data.code_display
               }
            ]
         };

         var subject = {
            reference: data.subject_reference_number,
            display: data.subject_display
         },
         encounter = {
            reference: data.encounter_reference_number
         };
         var onsetDateTime = data.onsetDateTime ;
         var recordedDate = data.recordedDate;


          var dataTemplete = {};
            dataTemplete = { 
              resourceType: data.resourceType,
              clinicalStatus :  clinicalStatus, 
              category :category,
              code : code,
              subject : subject,
              encounter : encounter,
              onsetDateTime :  onsetDateTime,
              recordedDate :  recordedDate
            
            };

          console.log('\r\nSend body tempelte ')
          console.log(JSON.stringify(dataTemplete))
          var options = {
            method: "POST",
            url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Condition",
            headers: {
              "Content-Type": "application/json",
              "Authorization" : authToken
            },
            body: dataTemplete,
            json: true
          };

          request(options, async function(error, response, rows) {
            console.log('eror' + error);
          //   try{
              if (error) {
                console.log("Error request")
                return callback({
                  success: false,
                  message: error,
                  payload: error
                });
              } else {
                return callback({
                      success: true,
                      message: "berhasil",
                      payload: rows
                });
              }
            
          });
        }
        else{
          return callback({
            success: false,
            message: "token tidak ditemukan"
          });
        }

  },
  OnCondition_Secondary: async function(req,data,callback) {
    if(req.headers.authorization !== undefined){
      console.log("req.headers.authorization",req.headers.authorization);
      // console.log("datas",req);
      const authToken = req.headers.authorization;


      var clinicalStatus = {
        coding : [
            {
                system : "http://terminology.hl7.org/CodeSystem/condition-clinical",
                code : data.clinicalStatus_code,
                display : data.clinicalStatus_display
            }
        ]

      }
      var category = [
        {
         coding : [
            {
               system : "http://terminology.hl7.org/CodeSystem/condition-category",
               code : data.category_code,
               display : data.category_display
            }
         ]
        }
      ]

      var code = {
        coding : [
           {
              system: "http://hl7.org/fhir/sid/icd-10",
              code: data.code_coding,
              display: data.code_display
           }
        ]
     };

     var subject = {
        reference: data.subject_reference_number,
        display: data.subject_display
     },
     encounter = {
        reference: data.encounter_reference_number,
        display : data.encounter_display

     };
     var onsetDateTime = data.onsetDateTime ;
     var recordedDate = data.recordedDate;


      var dataTemplete = {};
        dataTemplete = { 
          resourceType: data.resourceType,
          clinicalStatus :  clinicalStatus, 
          category :category,
          code : code,
          subject : subject,
          encounter : encounter,
          onsetDateTime :  onsetDateTime,
          recordedDate :  recordedDate
        
        };

      console.log('\r\nSend body tempelte ')
      console.log(JSON.stringify(dataTemplete))
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Condition",
        headers: {
          "Content-Type": "application/json",
          "Authorization" : authToken
        },
        body: dataTemplete,
        json: true
      };

      request(options, async function(error, response, rows) {
        console.log('eror' + error);
      //   try{
          if (error) {
            console.log("Error request")
            return callback({
              success: false,
              message: error,
              payload: error
            });
          } else {
            return callback({
                  success: true,
                  message: "berhasil",
                  payload: rows
            });
          }
        
      });
    }
    else{
      return callback({
        success: false,
        message: "token tidak ditemukan"
      });
    }

},


};
// })

