var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');

require('dotenv').config();
module.exports = {
  OnMedication: async function(req,data,callback) {
        if(req.headers.authorization !== undefined){
          console.log("req.headers.authorization",req.headers.authorization);
          // console.log("datas",req);
          const authToken = req.headers.authorization;
          var identifier =[];
          var ingredient_array = [];
          var extension_array = [];
          var meta = {
            profile : [
               data.meta_profile
            ]
          };
          identifier.push({
            system : data.identifier_system,
            use : data.identifier_use,
            value : data.identifier_value
          })
          var code = {
            coding : [
                {
                    system : data.code_coding_system ,
                    code : data.code_coding_code,
                    display : data.code_coding_display
                }
            ]

          }
          var status =  data.status;
          var manufacturer = {
            reference : data.manufacturer_reference
          };
          var form = {
            coding : [
                {
                    system : data.form_coding_system,
                    code : data.form_coding_code,
                    display : data.form_coding_display
                }
            ]

          };
          console.log("data.ingredient",data.ingredient);
          await loop(data.ingredient,async ingredients =>{
            ingredient_array.push({
                itemCodeableConcept : {
                    coding : [
                        {
                            system : ingredients.itemCodeableConcept_system,
                            code : ingredients.itemCodeableConcept_code,
                            display: ingredients.itemCodeableConcept_display
                        }
                    ]
                },
                isActive: data.isActive,
                strength : {
                    numerator : {
                        value : ingredients.strength_numerator_value,
                        system : ingredients.strength_numerator_system,
                        code : ingredients.strength_numerator_code
                    },
                    denominator : {
                        value : ingredients.strength_denominator_value,
                        system : ingredients.strength_denominator_system,
                        code : ingredients.strength_denominator_code
                    }
                }
              })
          })
          await loop(data.extension,async extensions =>{
            extension_array.push({
                url : extensions.url,
                valueCodeableConcept : {
                    coding: [
                        {
                            system: extensions.valueCodeableConcept_coding_system,
                            code : extensions.valueCodeableConcept_coding_code,
                            display: extensions.valueCodeableConcept_coding_display
                        }
                    ]
                } 
            })
          })
         
 

 


          var dataTemplete = {};
            dataTemplete = { 
              resourceType: data.resourceType,
              meta :meta,
              identifier : identifier,
              code : code,
              status : status,
              manufacturer : manufacturer,
              form : form,
              ingredient :ingredient_array,
              extension : extension_array

            
            };

          console.log('\r\nSend body tempelte ')
          console.log(JSON.stringify(dataTemplete))
          var options = {
            method: "POST",
            url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Medication",
            headers: {
              "Content-Type": "application/json",
              "Authorization" : authToken
            },
            body: dataTemplete,
            json: true
          };

          request(options, async function(error, response, rows) {
            console.log('eror' + error);
          //   try{
              if (error) {
                console.log("Error request")
                return callback({
                  success: false,
                  message: error,
                  payload: error
                });
              } else {
                return callback({
                      success: true,
                      message: "berhasil",
                      payload: rows
                });
              }
            
          });
        }
        else{
          return callback({
            success: false,
            message: "token tidak ditemukan"
          });
        }

  },
  OnCondition_Secondary: async function(req,data,callback) {
    if(req.headers.authorization !== undefined){
      console.log("req.headers.authorization",req.headers.authorization);
      // console.log("datas",req);
      const authToken = req.headers.authorization;


      var clinicalStatus = {
        coding : [
            {
                system : "http://terminology.hl7.org/CodeSystem/condition-clinical",
                code : data.clinicalStatus_code,
                display : data.clinicalStatus_display
            }
        ]

      }
      var category = [
        {
         coding : [
            {
               system : "http://terminology.hl7.org/CodeSystem/condition-category",
               code : data.category_code,
               display : data.category_display
            }
         ]
        }
      ]

      var code = {
        coding : [
           {
              system: "http://hl7.org/fhir/sid/icd-10",
              code: data.code_coding,
              display: data.code_display
           }
        ]
     };

     var subject = {
        reference: data.subject_reference_number,
        display: data.subject_display
     },
     encounter = {
        reference: data.encounter_reference_number,
        display : data.encounter_display

     };
     var onsetDateTime = data.onsetDateTime ;
     var recordedDate = data.recordedDate;


      var dataTemplete = {};
        dataTemplete = { 
          resourceType: data.resourceType,
          clinicalStatus :  clinicalStatus, 
          category :category,
          code : code,
          subject : subject,
          encounter : encounter,
          onsetDateTime :  onsetDateTime,
          recordedDate :  recordedDate
        
        };

      console.log('\r\nSend body tempelte ')
      console.log(JSON.stringify(dataTemplete))
      var options = {
        method: "POST",
        url: "https://api-satusehat-stg.dto.kemkes.go.id/fhir-r4/v1/Medication",
        headers: {
          "Content-Type": "application/json",
          "Authorization" : authToken
        },
        body: dataTemplete,
        json: true
      };

      request(options, async function(error, response, rows) {
        console.log('eror' + error);
      //   try{
          if (error) {
            console.log("Error request")
            return callback({
              success: false,
              message: error,
              payload: error
            });
          } else {
            return callback({
                  success: true,
                  message: "berhasil",
                  payload: rows
            });
          }
        
      });
    }
    else{
      return callback({
        success: false,
        message: "token tidak ditemukan"
      });
    }

},


};
// })

async function loop(items, callback) {
    for (var a = 0; a < items.length; a++) {
      // eslint-disable-next-line
      await callback(items[a]);
    }
  }
  










