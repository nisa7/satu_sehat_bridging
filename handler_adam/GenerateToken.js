var axios = require("axios");
const e = require("express");
var request = require("request");
var mysql = require('mysql');
const qs = require('querystring');
require('dotenv').config();
module.exports = {
  onGenerateToken: async function(data,callback) {
        var client_id = process.env.client_id;
        var client_secret = process.env.client_secret;
        const baseUrl = 'https://api-satusehat-dev.dto.kemkes.go.id/oauth2/v1/accesstoken';
        const queryParams = {
          grant_type : 'client_credentials'
        };
        const queryString = qs.stringify(queryParams);
        // const body = new URLSearchParams(formData);
        const apiUrl = `${baseUrl}?${queryString}`;
        const postData = {
          client_id: client_id,
          client_secret: client_secret
        };

        const requestBody = qs.stringify(postData);
        const headers = {
          'Content-Type': 'application/x-www-form-urlencoded',
        };
        
        var options = {
          method: "POST",
          url: apiUrl,
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          data : requestBody
        };
        axios.post(apiUrl, requestBody, { headers })
        .then(response => {
          console.log('Response:', response.data);   
          callback({
               success: true,
               message: "berhasil",
               payload: response.data
          });
          // Handle the response from the API
      
        })
        .catch(error => {

          // Handle any errors that occurred during the request
          console.log('Error:', error);
          callback({
              success: false,
              message: error,
              payload: error
          });
        });
        // request(options, async function(error, response, rows) {
        //   // console.log('response' + response.data);
        //   console.log('rows' + rows);
        //   console.log('eror' + error);
        //   try{
        //     if (error) {
        //       console.log("Error request")
        //       return callback({
        //         success: false,
        //         message: error,
        //         payload: error
        //       });
        //     } else {
        //       console.log("response",response);
        //       // console.log("rows",rows.data);
        //       return callback({
        //             success: true,
        //             message: "berhasil",
        //             payload: rows.data
        //       });
        //     }
        //   }
        //   catch(error){
        //     return callback({
        //       success: false,
        //       message: 'sambungan api terbitkan simrs terputus, cek api simrs',
        //       payload: 'sambungan api terbitkan simrs terputus, cek api simrs' + error
        //   });
        //   }
          
        // });
   
  }
};
// })
async function loop(items, callback) {
  for (var a = 0; a < items.length; a++) {
    // eslint-disable-next-line
    await callback(items[a]);
  }
}
