"use strict";
var Ajv = require("ajv");
var AjvParam = require("ajv");
const ajv = new Ajv({
  allErrors: true,
  jsonPointers: true,
  removeAdditional: true
});
require("ajv-errors")(ajv /*, {singleError: true} */);

/**
 * param akan khusus menggunakan coerceTypes yaitu mekanisme untuk bisa mengubah struktur tipe  dalam pengecekan ajv
 * hal ini dilakukana karena param akan bernilai string semua
 */
const ajvParam = new AjvParam({
  allErrors: true,
  jsonPointers: true,
  removeAdditional: true,
  coerceTypes: true
});
require("ajv-errors")(ajvParam /*, {singleError: true} */);

var schema = {
  type: "object",
  description: "query pencarian",
  required: ["success", "message"],
  properties: {
    success: {
      description: "success respond",
      type: "boolean"
    },
    message: {
      description: "message dari hari pemrosesan data",
      type: "string"
    }
  },
  if: { properties: { success: { const: true } } },
  then: {
    additionalProperties: false,
    required: ["success", "message", "payload"],
    properties: {
      success: {
        description: "success respond",
        type: "boolean"
      },
      message: {
        description: "message dari hari pemrosesan data",
        type: "string"
      },
      payload: {
        uniqueItems: true,
        type: "array",
        items: {
          description: "kode paket pemeriksaan ",
          required: ["no_reg_rs", "nama", "waktu_registrasi_rs"],
          type: "object",
          properties: {
            no_reg_rs: {
              type: "string"
            },
            nama: {
              type: "string"
            },
            waktu_registrasi_rs: {
              type: "string"
            }
          }
        }
      }
    }
  },
  else: {
    additionalProperties: false,
    required: ["success", "message"],
    properties: {
      success: {
        description: "success respond",
        type: "boolean"
      },
      message: {
        description: "message dari hari pemrosesan data",
        type: "string"
      }
    }
  }
};

module.exports = {
  checker: function(data, callback) {
    let validate = ajvParam.compile(schema);
    validate(data);
    if (validate.errors !== null) {
      console.log(
        `********************************\r\n
        json validate param error json validate at ` +
         validate.errors
      );
      return callback({
        success: false,
        message: "json validate param error json validate at" + validate.errors
      });
    } else {
      return callback(data);
    }
  }
};
